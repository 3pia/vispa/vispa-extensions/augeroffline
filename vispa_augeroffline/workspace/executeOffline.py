#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from lxml import etree as ET
import argparse
import os
import subprocess
import stat
import shlex
import shutil
import sys
import time
import copy


def set_module_options_recursive(node, option_name, value, unit, parents):
    if(len(parents) > 0):
        parent = parents.pop(0)
        c = node.find("./" + str(parent))
        if(c is None):
            c = ET.SubElement(node, str(parent))
        set_module_options_recursive(c, option_name, value, unit, parents)
        return 0
    else:
        c = node.find("./" + str(option_name))
        if(c is None):
            c = ET.SubElement(node, str(option_name))
        c.text = str(value)
        if(unit is not None):
            c.set("unit", str(unit))
        return 0


def setModuleOptions(bootstrap_root, module_name, module_options):
    """ module_options is an array of arrays with the form
    row = [moduleOption, newOptionValue, newOptionUnit, parents]
    newOptionUnit can be None
    parents are a list of parents, if no "parent" of option is the module
    option root node parents should be an empty list.
    """
    bootstrapOverride = bootstrap_root.find("parameterOverrides")
    node = bootstrapOverride.find(".//" + module_name)
    if(node is None):
        a = ET.SubElement(bootstrapOverride, "configLink", {"id": module_name})
        node = ET.SubElement(a, module_name)

    for option_name, value, unit, parents in module_options:
        set_module_options_recursive(node, option_name, value, unit, parents)


def getFormattedBootstrap(bootstrap_string):
    xmllinks = []
    last_position = 0
    while True:
        pos1 = bootstrap_string.find("&", last_position)
        pos2 = bootstrap_string.find(";", pos1)
        if (pos1 == -1) or (pos2 == -1):
            break
        if  (pos2 - pos1) > 50:
            print("warning: pos2 - pos1 > 50, this should not happen")
            break
        if bootstrap_string[pos2 + 1:pos2 + 2] != "/":
            xmllinks.append(bootstrap_string[pos1:pos2 + 1])
            bootstrap_string = bootstrap_string.replace(xmllinks[-1], "")
            last_position = pos1
        else:
            last_position = pos2

    parser2 = ET.XMLParser(remove_blank_text=True, remove_comments=False,
                           attribute_defaults=False, recover=False, resolve_entities=True)
    BootstrapRoot2 = ET.fromstring(bootstrap_string, parser2)
    BootstrapTree2 = BootstrapRoot2.getroottree()
    bootstrap_string = ET.tostring(BootstrapTree2, pretty_print=True, xml_declaration=True, encoding=docinfo.encoding)
    pos1 = bootstrap_string.find("<centralConfig>")
    tmp = "\n"
    for xmllink in xmllinks:
        tmp += xmllink + "\n"
    bootstrap_string = bootstrap_string[:pos1 - 1] + tmp + bootstrap_string[pos1 - 1:]
    return bootstrap_string


''' parse command line arguments '''
parser = argparse.ArgumentParser(description='Generate EventFileReader.xml out of command line options.',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--dataFiles', dest='data_files', metavar='file1', type=str,
                    nargs='+', help='list of input data files, if this option is not specified the EventFileReader.xml specified in the boostrap file is used.')
parser.add_argument('-b', dest='bootstrap', metavar='/path/to/bootstrap.xml',
                    default='bootstrap.xml', type=str, help='bootstrap.xml file')
parser.add_argument('--fileType', metavar='filetype', dest='data_file_type',
                    default='Offline', help='input file type, choose between Offline FDAS CDAS IoAuger CORSIKA CONEX CONEXRandom AIRES SENECA REAS RadioSTAR RadioMAXIMA RadioAERA RadioAERAroot ')
parser.add_argument('--outputPath', dest='output_path', metavar='/output/path/',
                    default='output', type=str, help='output path')
parser.add_argument('--ADSTOutputPath', dest='adst_output_path', metavar='/output/path/',
                    default=False, type=str, help='output path')
parser.add_argument('--outputFileName', dest='output_file_name', metavar='ADST.root',
                    type=str, default='ADST',
                    help='filename of ADST output file, the given name will be appended by "_$uuid.root"')
parser.add_argument('--userAugerOffline', dest='user_auger_offline', metavar='./userAugerOffline',
                    default='AugerOffline', type=str, help='path to Offline executable')
parser.add_argument('--uuid', dest='uuid', metavar='jobid', type=str, default='0001', help='the job uuid')
parser.add_argument('-j', dest='n_threads', metavar='nThreads', type=int, default=1,
                    help='number of cores to use')
parser.add_argument('--createJobFiles', dest='create_job_files', const=True, nargs='?',
                    type=bool, default=False, help='do not execute Offline directly but create sh files for batch job submission')
parser.add_argument('--preExecutionScript', dest='pre_execution_script', metavar='',
                    type=str, default="", help='path to script that will be executed before jobs execution')
parser.add_argument('--postExecutionScript', dest='post_execution_script', metavar='',
                    type=str, default="", help='path script that will be executed after jobs execution')
parser.add_argument('--moduleOption', dest='module_options', metavar='', nargs='+',
                    type=str, default=None, help='Module options that should be overridden in the bootstrap file. The syntax is \"ModuleName::ModuleOption::Value[unit]\". The specification of a unit is optional.\nIf you have a level of recursion in your module options you can specify it with \"ModuleName::ModuleOptionLevel1::ModuleOptionLevel2::Value[unit]\". If several jobs are created, an index in the job option can be specified with \"{i}\".')
parser.add_argument('--starPattern', dest='star_pattern', const=True, nargs='?',
                    type=bool, default=False, help='create detector according to star pattern simulations')


args = vars(parser.parse_args())
data_file_type = args['data_file_type']
data_files = args['data_files']
bootstrap_path = args['bootstrap']
output_path = os.path.abspath(args['output_path'])
adst_output_path = args['adst_output_path']
if adst_output_path:
    adst_output_path = os.path.abspath(adst_output_path)
else:
    adst_output_path = output_path
print("setting adst output path to %s" % adst_output_path)
if not os.path.exists(output_path):
    print("ERROR: ouputpath \"%s\" does not exist" % output_path)
    sys.exit(-1)
user_auger_offline = args['user_auger_offline']
_uuid = args['uuid']
star_pattern = args['star_pattern']
create_job_files = args['create_job_files']
pre_execution_script = ''
post_execution_script = ''
if create_job_files:
    pre_execution_script = [args['pre_execution_script']]
    if pre_execution_script[0]:
        pre_execution_script = open(pre_execution_script[0], 'r').readlines()
    post_execution_script = [args['post_execution_script']]
    if post_execution_script[0]:
        post_execution_script = open(post_execution_script[0], 'r').readlines()
else:
    print("jobs will be executed interactively, pre and post execution script will be ignored")

module_options_str = args['module_options']
module_names = []
module_options = []
if module_options_str:
    print("the following module options will be set in the bootstrap file:")
    for element in module_options_str:
        tmp = element.split("::")
        module_names.append(tmp[0])
        parents = tmp[1:-2]
        tmp_module_value = tmp[-1].split("[")
        module_value = tmp_module_value[0]
        if(len(tmp_module_value) == 1):
            module_value_unit = None
        elif(len(tmp_module_value) == 2):
            if(tmp_module_value[1][-1] == "]"):
                module_value_unit = tmp_module_value[1][:-1]
            else:
                print("ERROR: option unit not specified correctly (missing ])")
                sys.exit(-1)
        else:
            print("ERROR: option unit not specified correctly (too many [)")
            sys.exit(-1)
        module_options.append([tmp[-2], module_value, module_value_unit, parents])
        print("\tmodule: %s, option: %s, value: %s, unit: %s, parents: %s" % (tmp[0], tmp[-2], module_value, module_value_unit, str(parents)))

n_threads = args['n_threads']
if(n_threads > 1):
    if(data_files is None):
        print "ERROR: If multithreading should be used, the input files have to be specified as cmd arguments."
        sys.exit(-1)

    import multiprocessing
    n_cores = multiprocessing.cpu_count()
    if(n_threads > n_cores and not create_job_files):
        print "Requested number of threads exceeds number of available cpu cores. N threads will be set to %i." % n_cores
        n_threads = n_cores
    if(n_threads > len(data_files)):
        print "More parallel jobs then input files specified. Limiting the maximum number of input files to %i" % len(data_files)
        n_threads = len(data_files)


print "outputpath: ", output_path

XLINK_NAMESPACE = "http://www.auger.org/schema/types"
XLINK = "{%s}" % XLINK_NAMESPACE

XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance"
XSI = "{%s}" % XSI_NAMESPACE

# create bootstrap_uuid.xml
parser = ET.XMLParser(remove_blank_text=True, remove_comments=False, attribute_defaults=False, recover=True, resolve_entities=False)
BootstrapTree = ET.parse(bootstrap_path, parser)
BootstrapRoot = BootstrapTree.getroot()
docinfo = BootstrapTree.docinfo


offline_config_path = BootstrapRoot.get(XSI + "noNamespaceSchemaLocation")[:-14]
# create Eventfilereader.xml only if data files have been specified, otherwise use link of bootstrap file
popen = []
imin=0
imax=0
n_additional_files=len(data_files)-int(len(data_files) / n_threads)*n_threads
for i in xrange(n_threads):
    pattern = ""
    if(n_threads > 1):
        pattern = "_%i" % i
    filename_pattern = str(_uuid) + pattern

    if(data_files is not None):
        ''' create EventFileReader.xml '''
        eventFileReaderRoot = ET.fromstring("""
            <EventFileReader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:noNamespaceSchemaLocation="%s">
                <InputFileType></InputFileType>
                <InputFilenames></InputFilenames>
            </EventFileReader>""" % os.path.join(offline_config_path, 'EventFileReader.xsd'))
        EventFileReaderTree = eventFileReaderRoot.getroottree()

        eventFileReaderRoot.find("./InputFileType").text = data_file_type
        filenames_tag = eventFileReaderRoot.find("./InputFilenames")
        N = len(data_files) / n_threads
        imax = imin+N-1
        if n_additional_files>0:
            imax+=1
            n_additional_files-=1
        if(i == n_threads - 1):
            imax = len(data_files)
        for filename in data_files[imin:imax]:
            if(filenames_tag.text is None):
                filenames_tag.text = "\n\t" + filename + "\n"
            else:
                filenames_tag.text = filenames_tag.text + "\t" + filename + "\n"

        docinfo = EventFileReaderTree.docinfo
        fEventFileReader = open(os.path.join(output_path, filename_pattern + "_EventFileReader.xml"), "w")
        fEventFileReader.write(ET.tostring(EventFileReaderTree, pretty_print=True, xml_declaration=True, encoding=docinfo.encoding))
        fEventFileReader.close()

        config_link_event = BootstrapRoot.find(".//configLink[@id='EventFileReader']")
        # -------------------------------------- create config link if it does not exists
        if(config_link_event is None):
            config_link_event = ET.SubElement(BootstrapRoot.find("./centralConfig"), "configLink")
            config_link_event.set("id", "EventFileReader")
            config_link_event.set("type", "XML")

        config_link_event.set(XLINK + "href", os.path.join(output_path, filename_pattern + "_EventFileReader.xml"))

        if star_pattern:
            config_link_stationlist = ET.SubElement(BootstrapRoot.find("./centralConfig"), "configLink")
            config_link_stationlist.set("id", "RStationListXMLManager")
            config_link_stationlist.set("type", "XML")
            config_link_stationlist.set(XLINK + "href", os.path.join(output_path, filename_pattern + "_RStationList.xml"))

            # create StationList.xml
            reas_list_file = data_files[imin:imax][0].split(".")[0] + ".list"
            import numpy as np
            temp = np.genfromtxt(reas_list_file, usecols=(2, 3, 4), unpack=True)
            names = np.genfromtxt(reas_list_file, usecols=(5), unpack=True, dtype="S15")
            N = len(names)
            # covert to meters
            xx, yy, zz = -temp[1] / 100., temp[0] / 100., temp[2] / 100.
            core = [477256.66, 6099203.68, 1400]  # pampa amarilla UTM coordinates
            station_ids = np.arange(1000, 1000 + N, 1, dtype=np.int)

            # parse core position from reas file
            f_reas = open(data_files[imin:imax][0], "r")
            lines = f_reas.readlines()
            core_reas_x = 0.
            core_reas_y = 0.
            core_reas_z = 0.
            for line in lines:
                if(line.startswith("CoreEastingOffline")):
                    core_reas_x = float(line.split("=")[1].split(";")[0].strip())
                if(line.startswith("CoreNorthingOffline")):
                    core_reas_y = float(line.split("=")[1].split(";")[0].strip())
                if(line.startswith("CoreVerticalOfflinev")):
                    core_reas_z = float(line.split("=")[1].split(";")[0].strip())
            core[0] += core_reas_x
            core[1] += core_reas_y

            root = ET.fromstring("""
            <stationList xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="%s"></stationList>""" % os.path.join(offline_config_path, 'RStationList.xsd'))
            tree = root.getroottree()
            for i, station_id in enumerate(station_ids):
                a = ET.SubElement(root, "station", {'id': str(station_id)})
                northing = ET.SubElement(a, "northing", {"unit": "meter"})
                northing.text = "%.3f" % (yy[i] + core[1])
                easting = ET.SubElement(a, "easting", {"unit": "meter"})
                easting.text = "%.3f" % (xx[i] + core[0])
                altitude = ET.SubElement(a, "altitude", {"unit": "meter"})
                altitude.text = "%.3f" % zz[i]
                tmp = ET.SubElement(a, "name").text = "Star_%s" % names[i]
                tmp = ET.SubElement(a, "commission")
                tmp.text = "2000-01-01T00:00:00Z"
                tmp = ET.SubElement(a, "decommission")
                tmp.text = "2030-01-01T00:00:00Z"
                ET.SubElement(a, "inGrid").text = "1"
                ET.SubElement(a, "neighbours")
                ET.SubElement(a, "ellipsoid").text = "WGS84"
                ET.SubElement(a, "zone").text = "19"
                ET.SubElement(a, "band").text = "H"
                ET.SubElement(a, "nChannels").text = "2"
                ET.SubElement(a, "firstChannelId").text = "1"
                ET.SubElement(a, "lastChannelId").text = "2"
            docinfo = tree.docinfo
            fout = open(os.path.join(output_path, filename_pattern + "_RStationList.xml"), "w")
            fout.write(ET.tostring(tree, pretty_print=True, xml_declaration=True, encoding=docinfo.encoding))
            fout.close()
        imin=imax+1
    # ------------------------------------- change output path of RecDataWriter
    output_file_name = os.path.join(adst_output_path, filename_pattern + "_" + args['output_file_name'] + ".root")
    setModuleOptions(BootstrapRoot, "RecDataWriter", [["outputFileName", output_file_name, None, ["rootOutput"]]])

    # set module options
    if(len(module_options)):
        for j in xrange(len(module_options)):
            # add indices for module options
            current_module_option = copy.copy(module_options[j])
            if(module_options[j][1].find("{i}") != -1):
                current_module_option[1] = current_module_option[1].replace("{i}", str(i))
            setModuleOptions(BootstrapRoot, module_names[j], [current_module_option])

    # ------- copy modulesequence to output directory and modify link in bootstrap
    modulesequence_path = os.path.join(output_path, filename_pattern + "_ModuleSequence.xml")
    config_link_modulesequence = BootstrapRoot.find(".//configLink[@id='ModuleSequence']")
    current_path = config_link_modulesequence.get(XLINK + "href")
    shutil.copyfile(current_path, modulesequence_path)
    config_link_modulesequence.set(XLINK + "href", modulesequence_path)

    bootstrap_string = getFormattedBootstrap(ET.tostring(BootstrapTree, pretty_print=True, xml_declaration=True, encoding=docinfo.encoding))

    fBootstrap = open(os.path.join(output_path, filename_pattern + "_bootstrap.xml"), "w")
    fBootstrap.write(bootstrap_string)
    fBootstrap.close()

    cmd = user_auger_offline + " -b " + os.path.join(output_path, filename_pattern + "_bootstrap.xml")

    if(create_job_files):
        output = []
        output.extend(pre_execution_script)
        output_filename = os.path.join(output_path, filename_pattern + ".out")
        output.append(cmd + " > " + output_filename + " &> " + output_filename + "\n")
        output.extend(post_execution_script)
        job_filename = os.path.join(output_path, filename_pattern + ".sh")
        output_file = open(job_filename, 'w')
        output_file.writelines(filter(None, output))
        print("job file " + filename_pattern + ".sh has been created")
        output_file.close()
        os.chmod(job_filename, stat.S_IXUSR + stat.S_IWUSR + stat.S_IRUSR)
    else:
        print("executing command %s\n\tlog will be written to: %s\n\tADST file will be written to %s, " % (cmd, os.path.join(output_path, filename_pattern + ".out"), os.path.join(output_path, output_file_name)))
        fstdout = open(os.path.join(output_path, filename_pattern + ".out"), "w")
        cmd = shlex.split(cmd)
        popen.append(subprocess.Popen(cmd, stdout=fstdout, stderr=subprocess.STDOUT))
if create_job_files:
    sys.exit(0)

exitcodes = []
t_start = time.time()
thread_range = range(n_threads)


def cleanup():
    for i in thread_range:
        popen[i].kill()

import atexit
atexit.register(cleanup)

while True:
    if(len(thread_range) == 0):
        break
    time.sleep(1)
    import datetime
    sys.stdout.write("\rrunning... %s" % (str(datetime.timedelta(seconds=int(time.time() - t_start)))))
    sys.stdout.flush()
    for i in thread_range:
        if(not popen[i].poll() is None):
            exitcode = popen[i].poll()
            exitcodes.append(exitcode)
            print "\rjob %i finished with exit code %i (t = %s)\n" % (i, exitcode, str(datetime.timedelta(seconds=int(time.time() - t_start))))
            thread_range.remove(i)

sys.exit(min(exitcodes))
