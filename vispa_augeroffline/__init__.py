# -*- coding: utf-8 -*-

from vispa.server import AbstractExtension
from controller import AugerOfflineController
# from controllerEventbrowser import AugerOfflineEventbrowserController


class AugerOfflineExtension(AbstractExtension):

    def name(self):
        return "augeroffline"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(AugerOfflineController(self))
#         self.add_controller(AugerOfflineEventbrowserController(self))
        self.add_workspace_directoy()
