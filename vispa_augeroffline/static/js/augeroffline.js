define([ "vispa/views/center", "async", "jquery", "css!../css/styles" ], function(CenterView, async, $) {

  function basename(path) {
    if (path)
      return path.replace(/\\/g, '/').replace(/.*\//, '');
    else
      return path;
  }

  function dirname(path) {
    if (path)
      return path.replace(/\\/g, '/').replace(/\/[^\/]*$/, '');
    else
      return path;
  }

  var AugerOfflineView = CenterView._extend({

    init: function init(args) {
      var self = this;
      init._super.apply(this, arguments);
      //self.getLogger().info("init AugerOffline");
      //this.logger.info("init AugerOffline");

      //    this.name = 'AugerOffline';
      //    this.constructor = AugerOfflineContent;

      // attributes
      this.nodes = {};
      this.addMenuEntry("submitJob", {
        label: 'submit job',
        iconClass: 'glyphicon glyphicon-play',
        callback: function() {
          self.to_be_implemented();
          //                self.submitToJobSubmission();
        }
      });
      this.addMenuEntry("submitJobDesigner", {
        label: 'submit to job designer',
        iconClass: 'glyphicon glyphicon-play',
        callback: function() {
          self.to_be_implemented();
          //                self.submitToJobSubmission();
        }
      });
      this.addMenuEntry("openBootstrap", {
        label: 'open bootstrap.xml',
        iconClass: 'glyphicon glyphicon-folder-open',
        callback: function() {
          var args = {
            "path": "$AUGEROFFLINEROOT/share/auger-offline/doc/",
            callback: function(path) {
              self.bootstrap_path = path;
              self.openBootstrap($(".module-sequence", self.nodes.content), path);
            }
          };
          self.spawnInstance("file", "FileSelector", args);
        }
      });
      this.addMenuEntry("openModuleSequence", {
        label: 'open module sequence',
        iconClass: 'glyphicon glyphicon-folder-open',
        callback: function() {
          var args = {
            //		      "path": "$AUGEROFFLINEROOT/share/auger-offline/doc/",
            "path": "/home/cglaser/RWTH/Offline/RdHybridReconstruction_test_vispa/",
            callback: function(path) {
              self.moduleSequenceFilename = path;
              self.loadModuleSequence($(".module-sequence", self.nodes.content), path);
            }
          };
          self.spawnInstance("file", "FileSelector", args);
        }
      });
      this.addMenuEntry("saveXML", {
        label: 'save XML files',
        iconClass: 'glyphicon glyphicon-floppy-disk',
        callback: function() {
          self.saveXMLFiles($(".module-sequence", self.nodes.content));
        }
      });
      this.addMenuEntry("saveModuleSequence", {
        label: 'save ModuleSequence',
        iconClass: 'glyphicon glyphicon-floppy-disk',
        callback: function() {
          var args = {
            "path": self.moduleSequenceFilename,
            callback: function(path) {
              self.saveModuleSequence($(".module-sequence", self.nodes.content), path);
            }
          };
          self.spawnInstance("file", "FileSelector", args);
          // TODO
          //          $.Topic("ext.file.selector").publish({
          //            path: "$HOME/Offline",
          //            multimode: false,
          //            callback: function(file) {
          //              self.saveModuleSequence($(".module-sequence", self.nodes.content), file);
          //            }
          //          });
        }
      });
      this.addMenuEntry("resetModuleOptions", {
        label: 'reset all module options to default',
        iconClass: 'glyphicon glyphicon-repeat',
        callback: function() {
          self.resetAllModuleOptions();
        }
      });
      this.addMenuEntry("setInfoLevel", {
        label: 'set all infolevel to ...',
        iconClass: 'glyphicon glyphicon-chevron-right',
        callback: function() {
          self.setAllInfoLevelTo($(".module-sequence", self.nodes.content));
        }
      });
      this.addMenuEntry("close", {
        label: 'close',
        iconClass: 'glyphicon glyphicon-remove',
        callback: function() {
          $(".module-sequence").children().remove();
          $(".div-module-options").remove();
          self.moduleSequenceFilename = "";
        }
      });

      this.setLabel("Auger Offline");

    },

    guid: function() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      }
      return function() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      };
    }(),

    render: function(node) {
      var self = this;
      this.setLoading(true);
      self.nodes.content = $('<div />').addClass('offline-full-body').get(0);

      self.GET("open", function(err, data) {
        console.debug("unique id = " + data);
        self._unique_id = data;

        console.debug(self.getPreference("userModules"));
        console.debug(self.getPreference("moduleConfig"));
        console.debug(self.getPreference("detectorConfig"));

        var tasks = [];

        tasks.push(self.GET.bind(self, "initialize", JSON.stringify({
          unique_id: self._unique_id,
          moduleConfig: self.getPreference("moduleConfig"),
          detectorConfig: self.getPreference("detectorConfig")
        })));

        tasks.push(self.getTemplate.bind(self, "html/modulelist.html"));

        tasks.push(self.POST.bind(self, "get_available_modules", {
          unique_id: self._unique_id
        }));

        async.parallel(tasks, function(err, res) {
          var data = res[0][0];
          var data2 = res[1];
          var modulelist = res[2][0];

          console.debug(modulelist);
          // TODO
          /*if (data[0] == 0) {
            $.Topic('msg.log').publish(
              'info', {
              text: "AugerOffline extension successfully initialized!",
              icon: 'ui-icon-check'
            });
          } else {
            if (data[0] == 1) {
              $.Topic('msg.log')
                .publish(
                'warning', {
                text: "Not all module or detector configuration files could be found!",
                icon: 'ui-icon-notice'
              });
            } else {
              {
                $.Topic('msg.log')
                  .publish(
                  'warning', {
                  text: "A problem occured while initializing AugerOffline Extension!",
                  icon: 'ui-icon-notice'
                });
              }
            }
          } */

          // add template to html page
          $(data2).appendTo(self.nodes.content);
          var listModuleSequence = $(".module-sequence", self.nodes.content);

          // definition of steering symbols
          var steering_symbols = [ {
            'steering-symbol-name': 'loop',
            'class': 'loop'
          }, {
            'steering-symbol-name': 'try',
            'class': 'try'
          }];
          var directives = {
            'steering-symbol': {
              class: function(params) {
                return params.value + " " + this.class;
              }
            }
          }
          $(".steering-symbol-list", self.nodes.content).render(steering_symbols, directives, {
            debug: true
          });
          var directives = {
            'category-name': {
              href: function(params) {
                return "#" + this['category-name'].replace(/\s+/g, '');
              }
            },
            'panel-collapse': {
              id: function(params) {
                return this['category-name'].replace(/\s+/g, '');
              }
            }
          }
          $(".div-module-list", self.nodes.content).render(modulelist, directives, {
            debug: true
          });

          var div_left = $(".div-left", self.nodes.content);
          $(div_left).sortable({
            axis: "y",
            handle: "h3",
            stop: function(event, ui) {
              // IE doesn't register the blur when
              // sorting
              // so trigger focusout handlers to
              // remove .ui-state-focus
              ui.item.children("h3").triggerHandler("focusout");
            }
          });
          $(div_left).find('li').draggable({
            helper: "clone",
            revert: "invalid",
            iframeFix: true,
            connectToSortable: $(listModuleSequence)
          });

          /*
           *  // add offline modules
           * $.each($.parseJSON(self._config.moduleConfig),
           * function(key, value) { $("<h3>").text(key).appendTo(allModules);
           * var modules = $("<div />", { id : key
           * }).appendTo(allModules); var listOfModules = $("<ul />").addClass(
           * "dropfalse module-list").appendTo(modules);
           * self.addModuleList(listOfModules, key,
           * listModuleSequence); });
           *
           *
           *  // add "all" modules $("<h3>").text("All
           * Modules").appendTo(allModules); var modules = $("<div />", {
           * id : "All Modules" }).appendTo(allModules); var
           * listOfModules = $("<ul />").addClass( "dropfalse
           * module-list").appendTo(modules);
           * self.addModuleList(listOfModules, "All Modules",
           * listModuleSequence);
           *
           *
           *
           *  // user modules $("<h3>").text("User
           * Modules").appendTo(allModules);
           *
           * var userModules = $("<div />", { id : "userModules"
           * }).appendTo(allModules);
           *
           * $("<button />").button({ label : "add User Module"
           * }).click(function() { self.addUserModule();
           * }).appendTo(userModules); $("<button />").button({ label :
           * "set path to userAugerOffline" }).click(function() {
           * self.setUserAugerOffline(); }).appendTo(userModules);
           *
           * var listOfUserModules = $("<ul />").addClass( "dropfalse
           * module-list").appendTo(userModules);
           * $(self._config.userModules) .each( function(index,
           * element) { self.logger.debug(element); self.logger.debug(this);
           *
           * var li_2 = $("<li>") .css({ 'z-index' : 9 }) .addClass(
           * "ui-state-default ui-corner-all offline-module
           * user-module") .draggable({ helper : "clone", revert :
           * "invalid", iframeFix : true, connectToSortable :
           * $(listModuleSequence) }).appendTo(listOfUserModules); $( "<span
           * class='module-name' id='name'>" + element + "</span>").appendTo(li_2);
           * });
           */

          listModuleSequence.sortable({
            handle: ".sort-handle",
            stop: function(event, ui) {
              self.identLi($(ui.item).parent());
            },
            receive: function(event, ui) {
              console.log($(this).data());
              var currentItem = $(this).data()['ui-sortable'].currentItem;
              if (!$(currentItem).hasClass("in-module-sequence")) {
                if ($(currentItem).hasClass("steering-symbol")) {
                  var uid = self.guid();
                  console.debug("set uid = " + uid);
                  $(currentItem).attr("id", uid);
                  $(currentItem).find("#name").attr("title", "unbounded");
                  var li = $("<li />").addClass("list-group-item  steering-symbol-stop in-module-sequence").attr("id",
                      uid).insertAfter($(currentItem));
                  $("<span style='float: left;' class='glyphicon glyphicon-resize-vertical sort-handle'></span>")
                      .appendTo(li);
                  $("<span / >").text($(currentItem).text() + " stop").appendTo($(li));

                  if ($(currentItem).hasClass("loop")) {
                    $(currentItem).attr("value", "unbounded");
                    $("span:last", currentItem).text("loop (numTimes = unbounded)");
                  }
                }
                $(currentItem).addClass("in-module-sequence").removeClass("ui-draggable").hover(function() {
                  self.addOverlayOptions(this, listModuleSequence);
                }, function() {
                  $(this).find(".overlay-options").remove();
                }).prepend("<span style='float: left;' class='glyphicon glyphicon-resize-vertical sort-handle'></span>");
              }
              self.identLi($(currentItem).parent());
            },
            axis: "y"
          });
          // listModuleSequence.disableSelection();
          listModuleSequence.selectable({
            cancel: ".overlay-options,.sort-handle",
            create: function(event, ui) {
              console.log("selectable created");
            },
            selected: function(event, ui) {
              console.log("selected");
              if ($(ui.selected).hasClass("offline-module")) {
                console.debug("module-identifier: " + $(ui.selected).text());
                self.showModuleOptions($.trim($(ui.selected).text()));
              }
              if ($(ui.selected).hasClass("loop")) {
                self.showLoopOptions($(ui.selected));
              }

            },
            unselected: function(event, ui) {
              console.debug($(ui.unselected).text());
              //		          if ($(ui.unselected).hasClass("loop")) {
              //			          self.changeLoopTimes($(ui.unselected).find("#name"));
              //		          }
              //			      if ($(ui.unselected).hasClass("offline-module")) {
              //			          self.submitModuleOptions($("#" + self._id
              //			          + "_form_moduleoptions"), $(ui.unselected).text());
              //		          }
              $(".div-module-options").remove();
            }
          });
          //	        listModuleSequence.find("li").addClass("ui-corner-all").prepend(
          //	          "<div class='handle'><span class='ui-icon ui-icon-carat-2-n-s'></span></div>");

          // window.setTimeout(function() {
          // var kids =
          // $(".module-sequence").children().each(function(){console.log(this);});
          // }, 500)

          // $(listModuleSequence)
          // .children()
          // .hover(
          // function() {
          // $('<span />')
          // .addClass('ui-state-default ui-corner-all ui-icon
          // ui-icon-trash')
          // .css('float', 'right')
          // .appendTo(this)
          // .click(function() {
          // console.debug($(this).parent());
          // $(this)
          // .parent()
          // .remove();
          // });
          // },
          // // addControlButtons(this),
          // function () {
          // $(this).find("span").remove();
          // }
          // );

          //                  var divButton = $("<div />").addClass("div-button").appendTo(
          //                      self.nodes.content);
          //                  
          //                  $("<button />").button({
          //                    label : "generate module sequence"
          //                  }).click(function() {
          //                    self.generateModuleSequence($(listModuleSequence));
          //                  }).css({
          //                    "padding" : 5
          //                  }).appendTo(divButton);
          //                  
          // $("<button />").button({
          // label : "load module sequence"
          // }).click(function() {
          // self.loadModuleSequence(listModuleSequence);
          // $.Topic('msg.notify').publish({
          // text : "Foo Bar Test",
          // icon : 'ui-icon-check'
          // });
          // $.Topic('msg.log').publish('info', {
          // text : "Foo Bar Test",
          // icon : 'ui-icon-check'
          // });
          // }).css({
          // "padding" : 5
          // })
          // // .text("generate module sequence")
          // .appendTo(divButton);

          // $("<button />").button({
          // label : "open bootstrap.xml"
          // }).click(function() {
          // // $.Topic("ext.file.selector").publish({path:
          // "$AUGEROFFLINEROOT/share/auger-offline/doc/", multimode:
          // false,
          // $.Topic("ext.file.selector").publish({path: "$HOME/RWTH",
          // multimode: false,
          // callback: function(file) {
          // self.bootstrap_path = file;
          // self.openBootstrap(listModuleSequence, file);
          // }});
          // }).css({
          // "padding" : 5
          // })
          // .appendTo(divButton);

          //                  $("<button />").button({
          //                    label : "submit job"
          //                  }).click(function() {
          //                    self.submitToJobSubmission();
          //                  }).css({
          //                    "padding" : 5
          //                  }).appendTo(divButton);
          //                  
          //                  $("<button />").button({
          //                    label : "submit to job designer"
          //                  }).click(
          //                      function() {
          //                        
          //                        var optionList = [];
          //                        
          //                        // add default options
          //                        optionList.push([ {
          //                          name : "Input Files",
          //                          group : "EventFileReader",
          //                          optionString : '--dataFiles',
          //                          value : "file1 file2",
          //                          decorator : '',
          //                          active : true
          //                        } ]);
          //                        self.logger.debug("{command:executeOffline.py, optionList:"
          //                            + optionList + "}");
          //                        Vispa.extensionManager.getFactory("jobdesigner",
          //                            "jobmanagement")._create({
          //                          command : "executeOffline.py",
          //                          optionList : optionList
          //                        });
          //                      }).css({
          //                    "padding" : 5
          //                  }).appendTo(divButton);

          // $("<button />").button({
          // label : "save modulesequence"
          // }).click(function() {
          // $.Topic("ext.file.selector").publish({path: "$HOME/RWTH",
          // multimode: false,
          // callback: function(file) {
          // self.saveModuleSequence($(listModuleSequence), file);
          // }});
          // }).css({
          // "padding" : 5
          // }).appendTo(divButton);

          //                  $("<button />").button({
          //                    label : "save bootstrap.xml"
          //                  }).click(function() {
          //                    $.Topic("ext.file.selector").publish({
          //                      path : "$HOME/RWTH",
          //                      multimode : false,
          //                      callback : function(file) {
          //                        self.saveBootstrap(file);
          //                      }
          //                    });
          //                  }).css({
          //                    "padding" : 5
          //                  }).appendTo(divButton);
          //                  
          //                  $("<button />").button({
          //                    label : "save XML files"
          //                  }).click(function() {
          //                    self.saveXMLFiles($(listModuleSequence));
          //                  }).css({
          //                    "padding" : 5
          //                  }).appendTo(divButton);
          //                  
          // $("<button />").button({
          // label : "set all infolevel to"
          // }).click(function() {
          // self.setAllInfoLevelTo($(listModuleSequence));
          // }).css({
          // "padding" : 5
          // }).appendTo(divButton);

          //                  $("form").submit(function() {
          //                    $.Logger.info("sumbit button pressed");
          //                    alert("submit pressed");
          //                    return false;
          self.setLoading(false);
        });
      });

      $(node).append(this.nodes.content);
      return this;

    },

    addUserModule: function() {
      $.Topic("msg.prompt").publish({
        text: "name of user module",
        callback: function(name) {
        }
      });
    },

    setAllInfoLevelTo: function(listModuleSequence) {
      var self = this;

      self.getTemplate("html/set_infolevel_to.html", function(err, template) {
        var modal = $(template).modal({
          backdrop: false
        });

        $("#submit", modal).click(function() {
          if (self.transmitModuleSequence($(listModuleSequence)) != 0) {
            console.debug("Modulesequence is empty.");
            alert("Modulesequence is empty. Setting infolevel does not make sense.");
            $(modal).modal('hide');
            return 0;
          }
          var infolevel = $("#infolevel", modal).val();
          console.log(infolevel);
          if (isNaN(parseInt(infolevel))) {
            alert("Input invalid! Infolevel has to be an integer!");
            return 0;
          }
          self.POST("setAllInfoLevelTo", {
            unique_id: self._unique_id,
            infolevel: infolevel
          });
          $(modal).modal('hide');
        })
      });
      console.debug("creating dialog");
    },

    submitToJobSubmission: function() {
      var self = this;

      console.debug("submitToJobDialog()");

      self.getTemplate("submit_job_dialog.html", function(err, tmpl) {

        var submit_job_dialog = $(tmpl).dialog(
            {
              modal: false,
              draggable: true,
              width: 600,
              title: "select input files",
              resizable: true,
              buttons: [ {
                text: 'submit',
                icons: {
                  primary: 'ui-icon-play'
                },
                click: function() {
                  $(this).dialog('close');
                  var AugerOfflinePath = "";
                  self.GET("get_auger_offline_path", {
                    unique_id: self._unique_id
                  }, function(err, data) {
                    AugerOfflinePath = data;
                    var files = $("#filename", ".submit-job-dialog").attr("value");
                    var file_type = $("#filetype", ".submit-job-dialog").attr("value");
                    var output_folder = $("#output_folder", ".submit-job-dialog").attr("value");
                    var jobs = [ {
                      command: "python " + AugerOfflinePath + "/executeOffline",
                      arguments: [ "-b " + AugerOfflinePath + "/bootstrap.xml",
                          "--userAugerOffline " + AugerOfflinePath + "/userAugerOffline", "--fileType " + file_type,
                          "--dataFiles " + files ],
                      outputPath: output_folder
                    } ];
                    Vispa.extensionManager.getFactory("jobsubmission", "jobmanagement")._create({
                      jobs: jobs
                    });
                  });
                }
              } ]
            });

        $("#filename", submit_job_dialog).attr({
          value: self.input_file_names
        })
        $("#output_folder", submit_job_dialog).attr({
          value: dirname(self.bootstrap_path)
        });
        $("#filetype", submit_job_dialog).attr({
          value: basename(self.input_file_type)
        });

        $("#set_input_files", ".submit-job-dialog").button().click(function() {
          window.setTimeout(function() {
            $.Topic("ext.file.selector").publish({
              path: "$HOME/Offline",
              multimode: true,
              callback: function(files) {
                console.log(files);
                $("#filename", ".submit-job-dialog").attr({
                  value: files
                });
              }
            });
          }, 0);
        });

        $("#set_output_folder", ".submit-job-dialog").button().click(function() {
          $.Topic("ext.file.selector").publish({
            path: "$HOME/Offline",
            multimode: false,
            callback: function(files) {
              $("#output_folder", ".submit-job-dialog").attr({
                value: files
              });
            }
          });
        });

      });

      //        
      // var content = $("<div />");
      //        
      // var table = $("<table />").appendTo(content);
      //        
      // var body = $("<tbody />").appendTo(table);
      //        
      // var tr = $("<tr />").appendTo(body);
      // $("<th />").text("input files").appendTo(tr);
      //        
      // var input_files = $("<input />").attr({
      // id : "filename",
      // type : "text",
      // value : "filename",
      // size: 50,
      // alt : "filename"
      // }).appendTo($("<th />")).appendTo(tr);
      //        
      // $("<button />").button({
      // label : "open input files"
      // }).click(function() {
      // window.setTimeout(function() {
      // $.Topic("ext.file.selector").publish({path: "$HOME/RWTH", multimode:
      // true,
      // callback: function(files) {
      // console.log(files);
      // input_files.attr({value: files});
      // }});
      // },0);
      // }).appendTo($("<th />")).appendTo(tr);
      //        
      // // file type
      // tr = $("<tr />").appendTo(body);
      // $("<th />").text("file type").appendTo(tr);
      // var input_type = $("<input />").attr({
      // id : "filetype",
      // type : "text",
      // size: 50,
      // value : "Offline"
      // }).appendTo($("<th />")).appendTo(tr);
      //        
      // // output path
      // tr = $("<tr />").appendTo(body);
      // $("<th />").text("output folder").appendTo(tr);
      //        
      // var output_folder = $("<input />").attr({
      // id : "outputpath",
      // type : "text",
      // size: 50,
      // value : "output path"
      // }).appendTo($("<th />")).appendTo(tr);
      //        
      // $("<button />").button({
      // label : "set output folder"
      // }).click(function() {
      // $.Topic("ext.file.selector").publish({path: "$HOME/RWTH", multimode:
      // false,
      // callback: function(files) {
      // output_folder.attr({value: files});
      // }});
      // }).appendTo($("<th />")).appendTo(tr);
      //        

    },

    saveXMLFiles: function(module_list) {
      var self = this;
      self.getTemplate("html/save_xml_files_dialog.html", function(err, dialog_template) {
        var modal = $(dialog_template).modal({
          backdrop: false
        });
        $("#submit", modal).click(function() {
          self.input_file_names = $("#filename_input", modal).val();
          self.input_file_type = $("#filetype_input", modal).val();
          var output_folder = $("#output_folder", modal).val();
          var bootstrap_filename = basename($("#filename_bootstrap", modal).val());
          if (output_folder.slice(-1) != "/") {
            output_folder += "/";
          }
          self.bootstrap_path = output_folder + bootstrap_filename;
          self.moduleSequenceFilename = $("#filename_modulesequence", modal).val();
          self.event_file_reader_filename = $("#filename_eventfilereader", modal).val();
          var path_relative = $("#path_relative", modal).is(':checked');
          var module_sequence = self.generateModuleSequence(module_list);
          self.POST("saveXMLFiles", JSON.stringify({
            unique_id: self._unique_id,
            input_files: self.input_file_names,
            input_file_type: self.input_file_type,
            output_folder: output_folder,
            filename_bootstrap: bootstrap_filename,
            filename_modulesequence: self.moduleSequenceFilename,
            filename_eventfilereader: self.event_file_reader_filename,
            path_relative: path_relative,
            module_sequence: module_sequence
          }), function(err, data) {
            console.debug(data);
            alert(data.info);
            modal.close();
          });
        });
        // modal end

        console.debug(modal);
        // set default value
        console.log(self.bootstrap_path);
        console.debug("self.bootstrap_path: " + self.bootstrap_path);
        if (self.bootstrap_path) {
          $("#output_folder", modal).attr({
            value: dirname(self.bootstrap_path)
          });
        }
        if (self.moduleSequenceFilename) {
          $("#filename_bootstrap", modal).val(basename(self.bootstrap_path));
        }
        if (self.moduleSequenceFilename) {
          $("#filename_modulesequence", modal).val(basename(self.moduleSequenceFilename));
        }
        $("#filename_input", modal).attr({
          value: self.input_file_names
        });
        $("#filetype_input", modal).attr({
          value: self.input_file_type
        });
        if (self.event_file_reader_filename) {
          $("#filename_eventfilereader", modal).attr({
            value: basename(self.event_file_reader_filename)
          });
        }

        // / add functionality to buttons
        $("#set_output_folder", modal).click(function() {
          var args = {
            "path": "~/",
            callback: function(files) {
              $("#output_folder", modal).attr({
                value: files
              });
            }
          };
          self.spawnInstance("file", "FileSelector", args);
        });

        // $("#set_bootstrap_filename", ".save-xml-files-dialog")
        // .button()
        // .click(function() {
        // window.setTimeout(function() {
        // $.Topic("ext.file.selector").publish({path: "$HOME/RWTH",
        // multimode: false,
        // callback: function(files) {
        // $("#bootstrap_filename",".save-xml-files-dialog").attr({value:
        // files});
        // }});
        // },0);
        // });
        //          
        // $("#set_modulesequence_filename", ".save-xml-files-dialog")
        // .button()
        // .click(function() {
        // window.setTimeout(function() {
        // $.Topic("ext.file.selector").publish({path: "$HOME/RWTH",
        // multimode: false,
        // callback: function(files) {
        // $("#modulesequence_filename",".save-xml-files-dialog").attr({value:
        // files});
        // }});
        // },0);
        // });
        //          
        // $("#set_eventfilereader_filename",
        // ".save-xml-files-dialog")
        // .button()
        // .click(function() {
        // window.setTimeout(function() {
        // $.Topic("ext.file.selector").publish({path: "$HOME/RWTH",
        // multimode: false,
        // callback: function(files) {
        // $("#eventfilereader_filename",".save-xml-files-dialog").attr({value:
        // files});
        // }});
        // },0);
        // });

        $("#set_input_files", modal).click(function() {
          alert("currently not supported");
          return 0;
          var args = {
            "path": "$HOME/Offline",
            callback: function(files) {
              $("#filename_input", modal).attr({
                value: files
              });
            }
          };
          self.spawnInstance("file", "FileSelector", args);
        });

      });

    },

    transmitModuleSequence: function(moduleList) {
      var self = this;
      var vModuleList = self.generateModuleSequence(moduleList);
      if (vModuleList.length == 0) {
        console.debug("module sequence is empty.");
        return -1;
      }
      self.POST("setModuleSequence", JSON.stringify({
        unique_id: self._unique_id,
        moduleSequence: vModuleList
      }));
      return 0;
    },

    saveModuleSequence: function(moduleList, filepath) {
      var self = this;
      var vModuleList = self.generateModuleSequence(moduleList);
      self.POST("saveModuleSequence", JSON.stringify({
        unique_id: self._unique_id,
        moduleSequence: vModuleList,
        filepath: filepath
      }), function() {
        console.debug("ajax done;");
        return true;
      });
    },

    saveBootstrap: function(filepath) {
      self.POST("saveBootstrap", JSON.stringify({
        unique_id: self._unique_id,
        filepath: filepath
      }));
    },

    generateModuleSequence: function(moduleList) {
      var vModuleList = [];
      $.each($(moduleList).children(), function(index, element) {
        // skip disabled modules
        if ($(element).hasClass("disabled")) {
          return true;
        }
        var extraInfo = "";
        var moduleName = "";
        console.debug($(element));
        if ($(element).hasClass("loop")) {
          extraInfo = $(element).attr("value").trim().replace(/\s+/g, ' ');
          moduleName = $(element).text().trim().replace(/\s+/g, ' ');
        } else {
          moduleName = $(element).text().trim().replace(/\s+/g, ' ');
        }
        vModuleList.push([ moduleName, extraInfo ]);
        console.debug(index, "[" + moduleName + "," + extraInfo + "]");
      });
      return vModuleList;
    },

    openBootstrap: function(listModuleSequence, bootstrap_path) {
      var self = this;
      self.GET("openBootstrapXML", JSON.stringify({
        unique_id: self._unique_id,
        path: bootstrap_path
      }), function(err, data) {
        console.debug("ajax request returned:");
        console.debug("moduleSequence: " + data['moduleSequence']);
        moduleSequence = data.moduleSequence;
        self.input_file_names = data.input_file_names;
        console.debug("input_file_names: " + self.input_file_names);
        self.input_file_type = data.input_file_type;
        self.moduleSequenceFilename = data.moduleSequenceFilename;
        self.event_file_reader_filename = data.event_file_reader_filename;
        self.setModuleSequence(moduleSequence, listModuleSequence);
      });
    },

    // this function generates the html code for the modulesequence contained
    // in the variable "moduleSequence". The html node to put the html code in
    // is "listModuleSequence".
    setModuleSequence: function(moduleSequence, listModuleSequence) {
      var self = this;
      listModuleSequence.empty();

      vUids = [];
      for (var i = 0; i < moduleSequence.length; ++i) {
        var li = "";
        if (moduleSequence[i][0] == "loop") {
          var uid = self.guid();
          vUids.push(uid);
          li = $("<li>").attr({
            id: uid,
          }).addClass("list-group-item steering-symbol in-module-sequence loop").appendTo(listModuleSequence);
          //        $("<span id='numTimes'>numTimes=" + $(li).attr("title") + "</span>").appendTo(li);
          $("<div class='module-name' id='name' title='" + moduleSequence[i][1] + "'>" + moduleSequence[i][0]
                  + " (numTimes=" + moduleSequence[i][1] + ")</div>").prependTo(li);
        } else if (moduleSequence[i][0] == "try") {
          var uid = self.guid();
          vUids.push(uid);
          li = $("<li>").attr({
            id: uid
          }).addClass("list-group-item steering-symbol in-module-sequence try").appendTo(listModuleSequence);
          $("<div class='module-name' id='name' title='" + moduleSequence[i][1] + "'>" + moduleSequence[i][0]
                  + "</div>").prependTo(li);
        } else if (moduleSequence[i][0] == "loop stop" || moduleSequence[i][0] == "try stop") {
          var uid = vUids.pop();
          li = $("<li>").attr({
            id: uid
          }).addClass("list-group-item steering-symbol-stop in-module-sequence").appendTo(listModuleSequence);
          $("<div class='module-name' id='name' title='" + moduleSequence[i][1] + "'>" + moduleSequence[i][0]
                  + "</div>").prependTo(li);
        } else {
          li = $("<li>").addClass("list-group-item offline-module in-module-sequence").appendTo(listModuleSequence);
          $("<div class='module-name' id='name' title='" + moduleSequence[i][1] + "'>" + moduleSequence[i][0]
                  + "</div>").prependTo(li);
        }

        // add sort handler
        $(li).addClass("in-module-sequence").removeClass("ui-draggable").hover(function() {
          self.addOverlayOptions(this, listModuleSequence);
        }, function() {
          $(this).find(".overlay-options").remove();
        }).prepend("<span style='float: left;' class='glyphicon glyphicon-resize-vertical sort-handle'></span>");
      }
      self.identLi(listModuleSequence);
    },

    loadModuleSequence: function(listModuleSequence, module_sequence_path) {
      var self = this;
      var request = self.POST("loadModuleSequence", JSON.stringify({
        unique_id: self._unique_id,
        path: module_sequence_path
      }), function(err, moduleSequence) {
        self.setModuleSequence(moduleSequence, listModuleSequence);
      });
    },

    changeLoopTimes: function(nTimes) {
      var self = this;
      console.log(nTimes);
      if (!(!isNaN(parseInt(nTimes)) || nTimes == "unbounded")) {
        alert("Input invalid! NumTimes has to be an integer or \"unbounded\"");
        return 0;
      }
      $(obj).attr("title", nTimes);
      $(obj).next().text("numTimes=" + nTimes);
    },

    showLoopOptions: function(obj) {
      console.log("show loop options");
      var self = this;
      $(".module-options").remove();

      self.getTemplate("html/loop_options.html", function(err, template) {
        var modal = $(template).modal({
          backdrop: false
        });

        $("#nTimes", modal).val($(obj).attr("value"));
        $("#submit", modal).click(function() {
          var nTimes = $("#nTimes", modal).val();
          if (!(!isNaN(parseInt(nTimes)) || nTimes == "unbounded")) {
            alert("Input invalid! NumTimes has to be an integer or \"unbounded\"");
            return 0;
          }
          $(obj).attr("value", nTimes);
          $("span:last", obj).text("loop (numTimes = " + nTimes + ")");
          $(modal).modal('hide');
        })
      });
    },

    showModuleOptions: function(moduleName) {
      var self = this;
      $(".div-module-options", self.nodes.content).remove();

      var tasks = [];

      tasks.push(self.getTemplate.bind(self, "html/module_options.html"));

      tasks.push(self.POST.bind(self, "getModuleOptions", JSON.stringify({
        unique_id: self._unique_id,
        moduleName: moduleName
      })));

      async.parallel(tasks, function(err, res) {
        var template = res[0];
        var dictOfOptions = res[1][0];

        console.debug(dictOfOptions);
        var no_options = false;
        if (dictOfOptions == -1 || dictOfOptions.length == 0) {
          no_options = true;
          // FIXME
          //	        $.Topic('msg.log').publish('info', {
          //	          text: "Module " + moduleName + " has not options",
          //	          icon: 'ui-icon-notice'
          //	        });
        } else { // module has options
          var div_module_options = $(template).appendTo(".div-module-options-row", self.nodes.content);
          $(".div-module-options-row", self.nodes.content).removeClass("hidden");
          var directives = {
            'module-options': {
              depth: {
                class: function(params) {
                  console.log(params);
                  if (this.depth > 1) {
                    return "table-indent-" + (this.depth - 1);
                  }
                },
                depth: function(params) {
                  return this.depth;
                },
                text: function(params) {
                  return params.value;
                }
              },
              'option-name': {
                title: function(params) {
                  return this.comment;
                },
                parent: function(params) {
                  if (this.parent) {
                    return this.parent;
                  }
                }
              },
              value: {
                default_value: function(params) {
                  return this.default_value;
                },
                current_value: function(params) {
                  return this.value;
                },
                'sub-module-title': function(params) {
                  if (this.sub_module_title) {
                    return true;
                  }
                }
              },
              unit: {
                default_value: function(params) {
                  return this.default_unit;
                },
                current_value: function(params) {
                  return this.unit;
                }
              }
            }
          }
          $(div_module_options).render(dictOfOptions, directives, {
            debug: true
          });
        }
        // hide all fields with empty units
        $(div_module_options).find("[data-bind='unit']").each(function(index, element) {
          if ($(element).val() == "") {
            $(element).parent().addClass("hidden");
          }
        })
        // hide all value and unit fields for sub-module titles
        $(div_module_options).find("[data-bind='value']").each(function(index, element) {
          if ($(element).attr('sub-module-title') == "true") {
            $(element).parent().addClass("hidden");
          }
        })
        $(div_module_options).find("[data-bind='unit']").each(function(index, element) {
          if ($(element).attr('sub-module-title') == "true") {
            $(element).parent().addClass("hidden");
          }
        })

        // add default value to tooltip
        $(".module-option-revert", div_module_options).each(function(index, element) {
          var default_value = $(this).parent().find("input").attr('default_value');
          var title = $(element).attr('title');
          $(element).attr('title', title + " \"" + default_value + "\".");
        });
        // add fuctionality to revert buttons
        $(".module-option-revert", div_module_options).each(function(index, element) {
          $(element).click(function() {
            var input = $(this).parent().find("input");
            $(input).val($(input).attr('default_value'));
          })
        });
        // enable tooltips
        $(div_module_options).find("[data-toggle='tooltip']").tooltip({
          container: 'body'
        });

        // add functionality to buttons
        $("#save", div_module_options).click(function() {
          self.submitModuleOptions(div_module_options, moduleName);
          div_module_options.remove();
        });
        $("#close", div_module_options).click(function() {
          div_module_options.remove();
        });
        $("#reset", div_module_options).click(function() {
          self.resetModuleOptions(div_module_options);
        });

      });
    },

    showModuleOptions2: function(moduleName) {
      var self = this;
      // remove old module option div if exists
      $(".module-options").remove();

      // var request2 = $.ajax({
      // url:
      // Vispa.urlHandler.dynamic("/extensions/augeroffline/static/html/module_options.html"),
      // type: 'GET',
      // async: false,
      // success: function(response) {
      // module_option_template = response;
      // }
      // });
      //        
      // $.when(request, request2).done(
      // function(data1, data2) {
      //                
      // }
      // );

      self.POST("getModuleOptions", JSON.stringify({
        unique_id: self._unique_id,
        moduleName: moduleName
      }), function(err, data) {
        dictOfOptions = data;
        console.debug(dictOfOptions);
        var no_options = false;
        if (dictOfOptions == -1 || dictOfOptions.length == 0) {
          no_options = true;
          $.Topic('msg.log').publish('info', {
            text: "Module " + moduleName + " has not options",
            icon: 'ui-icon-notice'
          });
        }

        var divModuleOptions = $("<div />").addClass("module-options");

        if (no_options) {
          divModuleOptions.text("Module " + moduleName + " has no options.");
        } else {
          var form = $("<form />").attr({
            id: self._id + "_form_moduleoptions",
            name: moduleName
          }).appendTo(divModuleOptions);
          var table = $("<table />").appendTo(form);

          var thead = $("<tr />").appendTo($("<thead />").appendTo(table));
          $("<th \>").text("option").appendTo(thead);
          $("<th \>").text("value").appendTo(thead);
          $("<th \>").text("unit").appendTo(thead);

          var body = $("<tbody />").appendTo(table);

          // loop through option dictionary
          // sort dictionary
          var keys = Object.keys(dictOfOptions);
          keys.sort();
          $.each(keys, function(index, key) {
            var value = dictOfOptions[key]
            // check if option has additional childs
            if (!("value" in value)) {
              // value has additional childs
              var tr = $("<tr \>").appendTo(body);
              $("<th>" + key + "</th>").appendTo(tr);

              var parent = key;
              $.each(value, function(key, value) {
                console.debug("child " + key + ":" + value);
                console.debug(value);
                var tr = $("<tr \>").appendTo(body);
                $("<th style='padding-left:10px' />").text(key).appendTo(tr);

                // value
                var th = $("<th />").appendTo(tr);
                $("<input />").attr({
                  type: "text",
                  value: value["value"],
                  alt: value["default_value"],
                  current_value: value["value"],
                  default_value: value["default_value"],
                  name: key,
                  parent: parent
                }).appendTo(th);

                // revert button
                $("<span />").addClass('ui-icon ui-icon-arrowrefresh-1-w overlay-icon').attr({
                  title: "revert to default"
                }).click(function() {
                  var input = $(this).parent().find("input");
                  $(input).attr({
                    value: $(input).attr('default_value')
                  });
                  console.debug("reverting to " + $(input).attr('default_value'));
                }).appendTo(th);

                // info button
                console.log(value);
                if ("comment" in value) {
                  var getClick = function(comment) {
                    return function() {
                      alert(comment);
                      //                $.Topic('msg.info').publish(comment);
                    };
                  };

                  $("<span />").addClass('ui-icon ui-icon-info overlay-icon').attr({
                    title: "info"
                  }).click(getClick(value['comment'])).appendTo(th);
                }

                // unit
                if (value["unit"]) {
                  var th = $("<th />").appendTo(tr);
                  $("<input />").attr({
                    type: "text",
                    value: value["unit"],
                    alt: value["default_unit"],
                    current_value: value["unit"],
                    default_value: value["default_unit"],
                    name: "unit"
                  }).appendTo(th);
                  $("<span />").addClass('ui-corner-all ui-icon ui-icon-arrowrefresh-1-w overlay-icon').attr({
                    title: "revert to default"
                  }).click(function() {
                    var input = $(this).parent().find("input");
                    $(input).attr({
                      value: $(input).attr('default_value')
                    });
                    console.debug("reverting to " + $(input).attr('default_value'));
                  }).appendTo(th);
                } else {
                  $("<th />").appendTo(tr);
                }
              });
            } else {

              var tr = $("<tr \>").appendTo(body);

              $("<th />").text(key).appendTo(tr);

              // value
              var th = $("<th />").appendTo(tr);
              console.debug("value: " + value["value"]);
              console.debug("comment: " + value["comment"]);
              if (value["value"] != "") {
                $("<input />").attr({
                  type: "text",
                  value: value["value"],
                  alt: value["default_value"],
                  current_value: value["value"],
                  default_value: value["default_value"],
                  name: key

                }).appendTo(th);
              } else {
                console.debug("value is = \"\"");
                $("<input />").attr({
                  type: "text",
                  value: "",
                  alt: "",
                  name: key

                }).appendTo(th);
              }

              // revert button
              $("<span />").addClass('ui-icon ui-icon-arrowrefresh-1-w overlay-icon').attr({
                title: "revert to default"
              }).click(function() {
                var input = $(this).parent().find("input");
                $(input).attr({
                  value: $(input).attr('default_value')
                });
                console.debug("reverting to " + $(input).attr('default_value'));
              }).appendTo(th);

              // info button
              if ("comment" in value) {
                var getClick = function(comment) {
                  return function() {
                    alert(comment);
                    //                $.Topic('msg.info').publish(comment);
                  };
                };

                $("<span />").addClass('ui-icon ui-icon-info overlay-icon').attr({
                  title: "info"
                }).click(getClick(value['comment'])).appendTo(th);
              }

              // unit
              if (value["unit"]) {
                var th = $("<th />").appendTo(tr);
                $("<input />").attr({
                  type: "text",
                  value: value["unit"],
                  alt: value["default_unit"],
                  current_value: value["unit"],
                  default_value: value["default_unit"],
                  name: "unit"
                }).appendTo(th);
                $("<span />").addClass('ui-icon ui-icon-arrowrefresh-1-w overlay-icon').attr({
                  title: "revert to default"
                }).click(function() {
                  var input = $(this).parent().find("input");
                  $(input).attr({
                    value: $(input).attr('default_value')
                  });
                  console.debug("reverting to " + $(input).attr('default_value'));
                }).appendTo(th);
              } else {
                $("<th />").appendTo(tr);
              }
            }
          });
          // override submit function
          form.submit(function(event) {
            event.preventDefault();
            // event.stopPropagation();
            self.submitModuleOptions(this, moduleName);
            divModuleOptions.remove();
            return false;
          });

          //table.tablesorter();
        }
        divModuleOptions.dialog({
          modal: false,
          draggable: true,
          width: 600,
          title: "Module options for module " + moduleName,
          resizable: true,
          buttons: [ {
            text: 'save',
            icons: {
              primary: 'ui-icon-disk'
            },
            click: function() {
              form.submit();
            }
          }, {
            text: 'reset',
            alt: 'reset all values to default',
            icon: {
              primary: 'ui-icon-close'
            },
            click: function() {
              self.resetModuleOptions(this);
            }
          }, {
            text: 'close',
            icon: {
              primary: 'ui-icon-close'
            },
            click: function() {
              $(this).dialog('close');
            }
          } ]
        });

        // $("<input />").button({
        // label : "save"
        // }).attr({
        // type : 'submit'
        // }).appendTo(form);
        //                  
        // $("<input />").button({
        // label : "reset",
        // alt : "reset all values to default"
        // }).click(function() {
        // self.resetModuleOptions(this);
        // }).appendTo(form);
        //                  
        // $("<input />").button({
        // label : "close"
        // }).click(function() {
        // divModuleOptions.remove();
        // }).appendTo(form);

      });
    },

    resetModuleOptions: function(obj) {
      $(obj).parent().find("input").each(function(index, element) {
        $(element).val($(element).attr('default_value'));
      });
    },

    resetAllModuleOptions: function() {
      var self = this;
      self.POST("resetAllModuleOptions", {
        unique_id: self._unique_id
      }, function(err, data) {
        if (data == 0) {
          // FIXME: log msgs
          //        $.Topic('msg.log').publish('info', {
          //          text: "all module options reset",
          //          icon: 'ui-icon-check'
          //        });
          self.logger.debug("All module options have been reset succesful.");
        } else {
          //        $.Topic('msg.log').publish('info', {
          //          text: "a problem occured while reseting module options",
          //          icon: 'ui-icon-alert'
          //        });
          self.logger.debug("Error in resetting all module options");
        }
      });
    },

    identLiSingle: function(obj) {
      console.log("identLi");
      var prev = $(obj).prevAll();
      var nLoops = 0;
      $.each(prev, function(index, element) {
        if ($(element).hasClass("steering-symbol")) {
          nLoops += 1;
        }
        if ($(element).hasClass("steering-symbol-stop")) {
          nLoops -= 1;
        }
      });
      if ($(obj).hasClass("steering-symbol-stop")) {
        nLoops -= 1;
      }

      $(obj).removeClass(function(index, css) {
        return (css.match(/(^|\s)in-loop-\S+/g) || []).join(' ');
      });

      if (nLoops > 0) {
        $(obj).addClass("in-loop-" + nLoops);
      }
    },

    identLi: function(obj) {
      var self = this;
      $.each($(obj).children(), function(index, element) {
        self.identLiSingle(element);
      });
    },

    to_be_implemented: function() {
      $("<div>This feature will be implemented soon!</div>").dialog({
        modal: true,
        title: 'info',
        buttons: {
          Ok: function() {
            $(this).dialog("close");
          }
        }
      });
    },

    submitModuleOptions: function(form, moduleName) {
      var self = this;
      console.debug("moduleName = " + moduleName);
      var changedModuleOptions = [];
      // $("#" + self._id + "_form_moduleoptions")
      $(form).find($("tbody")).children().each(function(index, element) {
        var moduleOption = $(element).find("[data-bind='option-name']").text();
        var oldOptionValue = $(this).find("input[data-bind='value']").attr("current_value");
        var defaultOptionValue = $(this).find("input[data-bind='value']").attr("default_value");
        var newOptionValue = $(this).find("input[data-bind='value']").val();
        var newOptionUnit = $(this).find("input[data-bind='unit']").val();
        var oldOptionUnit = $(this).find("input[data-bind='unit']").attr("current_value");
        var defaultOptionUnit = $(this).find("input[data-bind='unit']").attr("default_value");
        var parent = $(this).find("td[data-bind='option-name']").attr("parent");
        console.debug(newOptionValue + "  " + oldOptionValue);
        console.debug(newOptionUnit + "  " + oldOptionUnit);
        console.log(oldOptionUnit);
        if (!oldOptionUnit) {
          if ((newOptionValue != oldOptionValue)) {
            row = [ moduleOption, newOptionValue, null, parent ];
            changedModuleOptions.push(row);
          }
          ;
        } else {
          if ((newOptionValue != oldOptionValue) || (newOptionUnit != oldOptionUnit)) {
            row = [ moduleOption, newOptionValue, newOptionUnit, parent ];
            changedModuleOptions.push(row);
          }
          ;
        }
        ;
      });
      console.debug(changedModuleOptions);
      if (changedModuleOptions.length > 0) {
        console.debug(JSON.stringify(changedModuleOptions));
        self.POST("setModuleOptions", JSON.stringify({
          unique_id: self._unique_id,
          moduleName: moduleName,
          moduleOptions: changedModuleOptions
        }), function(err, data) {
          if (data['result'] == 0) {
            // TODO
            //          $.Topic('msg.log').publish('info', {
            //            text: "module options saved",
            //            icon: 'ui-icon-check'
            //          });
          }
        });
      }
      ;
      return true;
    },

    addOverlayOptions: function(obj, listModuleSequence) {
      var self = this;
      var div = $('<div />').addClass("overlay-options").appendTo(obj);

      $('<span />').addClass('glyphicon glyphicon-remove overlay-icon').click(function() {
        if ($(obj).hasClass("steering-symbol")) {
          var id = $(obj).attr("id");
          $("#" + id).remove();
          $("#" + id).remove();
        } else {
          $(obj).remove();
          var module_options_name = $(".div-module-options").find("[data-bind='module-name']").text();
          var module_name = $(obj).find("[data-bind='module-identifier']").text();
          if (module_options_name == module_name) {
            $(".div-module-options").remove();
          }
        }

        window.setTimeout(function() {
          $(".module-options").remove();
        }, 100);

        self.identLi(listModuleSequence);
      }).appendTo(div);

      if (!$(obj).hasClass("steering-symbol")) {
        $('<span />').addClass('glyphicon glyphicon-ban-circle overlay-icon').click(function() {
          $(obj).toggleClass("disabled");
        }).appendTo(div);
      }
    },

    addControlButtons: function(obj) {
      console.log(obj);
      $('<span />').addClass('glyphicon glyphicon-remove').css('float', 'right').appendTo(obj).click(function() {
        $(obj).parent().remove();
      });
      return;
    },

    getAvailableModules: function() {
      var self = this;
      self.GET("get_available_modules", JSON.stringify({
        unique_id: self._unique_id
      }), function(err, data) {
        return data;
      });
    },
    addModuleList: function(listOfModules, category, listModuleSequence) {
      var self = this;
      self.GET("getListOfModules", JSON.stringify({
        unique_id: self._unique_id,
        category: category
      }), function(err, data) {
        moduleNames = data['listOfModules'];

        $.each(moduleNames, function(index, moduleName) {

          li = $("<li>").addClass("offline-module").draggable({
            helper: "clone",
            revert: "invalid",
            iframeFix: true,
            connectToSortable: $(listModuleSequence)
          }).appendTo(listOfModules);
          $("<span class='module-name'>" + moduleName + "</span>").appendTo(li);
        });
      });

    },

    applyConfig: function() {

      $("<div>You need to reload the page for the changes to take effect.</div>").dialog({
        modal: true,
        title: 'info',
        buttons: {
          Ok: function() {
            $(this).dialog("close");
          }
        }
      })
      // backgroundColor
      // $(this.nodes.content).css('background-color',
      // this._config.backgroundColor);
      // var request = $.ajax({
      // url : Vispa.urlHandler.dynamic("extensions/augeroffline/initialize"),
      // type : "POST",
      // data : {
      // workspace_id : Vispa.workspaceManager.getWorkspace().id,
      // moduleConfig : this._config.moduleConfig,
      // detectorConfig : this._config.detectorConfig
      // }
      // });
      // $.when(request).done(
      // function(data) {
      // if (data == 0) {
      // $.Topic('msg.notify').publish({
      // text : "AugerOffline extension successfully initialized",
      // icon : 'ui-icon-check'
      // });
      // } else if (data == 1) {
      // alert("One or more module config files can not be read in. The
      // modules that are contained in this file will not be available.");
      // $
      // .Topic('msg.notify')
      // .publish(
      // {
      // text : "One or more module config files can not be read in. The
      // modules that are contained in this file will not be available.",
      // icon : 'ui-icon-alert'
      // });
      // } else if (data == 2) {
      // alert("All module config files can not be read in. No modules will be
      // available.");
      // $
      // .Topic('msg.notify')
      // .publish(
      // {
      // text : "All module config files can not be read in. No modules will
      // be available.",
      // icon : 'ui-icon-alert'
      // });
      // } else {
      // $
      // .Topic('msg.notify')
      // .publish(
      // {
      // text : "A problem occured when initalizing AugerOffline extension.",
      // icon : 'ui-icon-alert'
      // });
      // }
      //                  
      // });
    }

  //  ready: function() {
  //    return this;
  //  },
  });

  return AugerOfflineView;

});