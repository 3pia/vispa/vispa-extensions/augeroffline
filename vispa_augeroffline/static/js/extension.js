define(["vispa/extensions", "./augeroffline"],
    function(Extensions, AugerOfflineView) {

  var AugerOfflineExtension = Extensions.Extension._extend({
  
    init: function init() {
      var self = this;
      init._super.call(this, "augeroffline");
    
      this.addViews({
        "augerofflineCenter": AugerOfflineView,
      });
    
      this.setOptions(AugerOfflineView, {
          maxInstances: 1
      });
    
    
      this.addMenuEntry("Auger-Offline", function(workspaceId) {
        self.createInstance(workspaceId, AugerOfflineView);
      });
      
      var moduleConfigObj = {
          "Radio Modules": "standardRdModuleConfig.xml",
          "SD Reconstruction Modules": "standardSdRecModuleConfig.xml",
          "Hybrid Reconstruction Modules": "standardHdRecModuleConfig.xml",
          "SdHAS Modules": "standardSdHASRecModuleConfig.xml",
          "Fd Modules": "standardFdRecModuleConfig.xml"
      };
      
      var detectorConfigObj = {
          "standardSdRealDetConfig": "standardSdRealDetConfig.xml",
          "standardFdRealDetConfig": "standardFdRealDetConfig.xml",
          "standardRdRealDetConfig": "standardRdRealDetConfig.xml"
      };
    
      this.setDefaultPreferences(AugerOfflineView, {
        moduleConfig: {
          type: 'object',
          value: moduleConfigObj,
          description: 'Filename of the module config xml files. The xml file needs to be in the folder \"$AUGEROFFLINEROOT/share/auger-offline/config/\". Only the modules specified in this xml files will be available in the Offline extension.'
        }, 
      detectorConfig: {
          type: 'object',
          value: detectorConfigObj,
          description: 'Filename of the detector config xml files. The xml file needs to be in the folder \"$AUGEROFFLINEROOT/share/auger-offline/config/\". Only the detector config specified in this xml files will be written to the bootstrap.xml file.'
      },
        userModules: {
        type: 'list',
        value: ["userModuleA", "userModuleB"],
          description: 'User Modules'
        }
      }, {
        title: "Auger Offline"
      });
    }
  });
  
  return AugerOfflineExtension;
 
});
