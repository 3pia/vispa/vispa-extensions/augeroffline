# -*- coding: utf-8 -*-

# imports
import cherrypy
import logging

import vispa.workspace
from vispa.controller import AbstractController

import collections
import ast
import os
import uuid
import json

logger = logging.getLogger(__name__)


class AugerOfflineController(AbstractController):

    def _augeroffline(self, unique_id):
        return self.extension.get_workspace_instance("AugerOffline", key=unique_id)

    def __init__(self, extension):
        AbstractController.__init__(self)
        self.extension = extension

    def __convert_unicode_to_string(self, data):
        ''' converts a unicode dictionary into string dictionary '''
        if isinstance(data, unicode):
            return str(data)
        elif isinstance(data, collections.Mapping):
            return dict(map(self.__convert_unicode_to_string, data.iteritems()))
        elif isinstance(data, collections.Iterable):
            return type(data)(map(self.__convert_unicode_to_string, data))
        else:
            return data

    @cherrypy.expose
    def open(self):
        unique_id = str(uuid.uuid4())
        return unique_id

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def initialize(self, unique_id, moduleConfig, detectorConfig):
#         logger.debug(kwargs)
#         unique_id, moduleConfig, detectorConfig = kwargs.unique_id, kwargs.moduleConfig, kwargs.detectorConfig
        logger.debug("initialize()")

#         json = cherrypy.request.json
#         moduleConfig = json[u'moduleConfig']
#         detectorConfig = json[u'detectorConfig']
#         unique_id = json[u'unique_id']
#         moduleConfig = self.__convert_unicode_to_string(moduleConfig)
#         detectorConfig = self.__convert_unicode_to_string(detectorConfig)

        logger.debug("initialize(): " + str(moduleConfig))
        logger.debug("type = " + str(type(moduleConfig)))

        # db test
        # db = self.get('db')
        # profile_id = self.get('profile_id')
        # preferences = ExtensionPreference.get_data_by_profile_id(db, profile_id, key='offline-full', parse_json=True)
        # default_preferences = {"foo": "bar"}
        # preferences = preferences or default_preferences
        # print preferences, "\n\n\n"
        # test end

        r = self._augeroffline(unique_id)
        tmp = r.initialize(str(moduleConfig), str(detectorConfig))
        logger.debug("tmp is " + str(tmp))
        return tmp

    @cherrypy.expose
    def resetAllModuleOptions(self, unique_id):
        logger.debug("resetAllModuleOptions")
        r = self._augeroffline(unique_id)
        return r.resetAllModuleOptions()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def setAllInfoLevelTo(self, unique_id, infolevel):
        r = self._augeroffline(unique_id)
        logger.debug("setAllInfoLevelTo " + infolevel)
        return r.setAllInfoLevelTo(int(infolevel))

    @cherrypy.expose
    def get_auger_offline_path(self, unique_id):
        r = self._augeroffline(unique_id)
        path = r.get_auger_offline_path()
        logger.debug("offline path = " + str(path))
        return path

    # deprecated
    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def getListOfModules(self, unique_id, category):
        logger.debug("getListOfModule(): category = " + category)
        logger.debug("userid = " + str(cherrypy.request.user.id))
        #      logger.debug("workspace id = ", cherrypy.request.workspace.id)

        r = self._augeroffline(unique_id)
        logger.debug("rpc connection established")

        listOfModules = r.getListOfModules(category)

        logger.debug("getListOfRadioModules for category " + category + "\n" + str(listOfModules))

        return listOfModules

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_available_modules(self, unique_id):
        r = self._augeroffline(unique_id)
        listOfModules = r.get_available_modules()
        return listOfModules

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def getModuleOptions(self, unique_id, moduleName):
        logger.info("getModuleOptions()")
        moduleName = moduleName.strip()
        logger.debug(moduleName)
        r = self._augeroffline(unique_id)
        return r.getModuleOptions(moduleName)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def openBootstrapXML(self, unique_id, path):
        r = self._augeroffline(unique_id)
        return r.openBootstrapXML(path)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def setModuleOptions(self, unique_id, moduleName, moduleOptions):
        logger.info("setModuleOptions")
        logger.debug("moduleName: " + moduleName)
        for moduleOption in moduleOptions:
            logger.debug(moduleOption[0] + " = " + moduleOption[1] + "[" + str(moduleOption[2]) + "]")

        r = self._augeroffline(unique_id)

        result = r.setModuleOptions(moduleName, moduleOptions)
        return result

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def saveModuleSequence(self, moduleSequence, filepath, unique_id):
        r = self._augeroffline(unique_id)
        r.setModuleSequence(moduleSequence)
        r.saveModuleSequence(filepath)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def setModuleSequence(self, moduleSequence, unique_id):
        r = self._augeroffline(unique_id)
        r.setModuleSequence(moduleSequence)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def saveBootstrap(self, filepath, unique_id):
        r = self._augeroffline(unique_id)
        r.saveBootstrap(filepath)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def saveXMLFiles(self, unique_id, input_files, input_file_type, output_folder,
                     filename_bootstrap, filename_modulesequence, filename_eventfilereader,
                     path_relative, module_sequence):
        filename_bootstrap = os.path.join(output_folder, filename_bootstrap)
        filename_modulesequence = os.path.join(output_folder, filename_modulesequence)
        filename_eventfilereader = os.path.join(output_folder, filename_eventfilereader)
        if(path_relative):
            bootstrap_dirname = os.path.dirname(filename_bootstrap)
            filename_modulesequence = os.path.relpath(filename_modulesequence, bootstrap_dirname)
            filename_eventfilereader = os.path.relpath(filename_eventfilereader, bootstrap_dirname)

        r = self._augeroffline(unique_id)

        tmp_return = r.saveBootstrap(output_folder, filename_bootstrap, filename_modulesequence, filename_eventfilereader)
        logger.debug("return = %i" % tmp_return)
        if(tmp_return != 0):
            if(tmp_return == 1):
                return {'sucess': False,
                        'info': "The specified folder(s) does not exist!"}
            else:
                return {'sucess': False,
                        'info': "An error occurred during saving XML the files!"}

        r.setModuleSequence(module_sequence)
        r.saveModuleSequence(os.path.join(output_folder, filename_modulesequence))
        r.saveEventFileReader(os.path.join(output_folder, filename_eventfilereader), input_files, input_file_type)
        return {'sucess': True,
                'info': "XML files saved correctly."}

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def loadModuleSequence(self, unique_id, path):
        r = self._augeroffline(unique_id)
        r.openModuleSequenceXML(path)
        moduleSequence = r.returnModuleSequence()

        logger.debug("moduleSequence: " + str(moduleSequence))

        return moduleSequence

#    def openModuleSequenceXML(self, path):
#      logger.info("openModuleSequenceXML()")
#      logger.debug("path to modulesquence file: ", path)
#
#      if(moduleName[0:2] == "Rd"):
#        for child in self.__RdModuleConfigRoot:
#          name = child.attrib.get('id')
#
#          if(name == moduleName):
#            modulePath = child.attrib.items()[2][1]
#            logger.debug("found module "+ moduleName + " at " + modulePath)
#
#            moduleOptionsTree = ET.parse(modulePath, self.__parser)
#            moduleOptionsRoot = moduleOptionsTree.getroot()
#
# load validation schema
# xmlschema_path = ModuleOptionsRoot.items()[0][1]
# xmlschema_doc = ET.parse(xmlschema_path)
# print "reading in XSD Schema from ", xmlschema_path
# xmlschema = ET.XMLSchema(xmlschema_doc)
# xmlschema.validate(ModuleOptionsTree)
#
#            listOfOptions = [];
#            logger.debug(moduleOptionsRoot)
#            for option in moduleOptionsRoot:
#              option = [option.tag,option.text,option.get('unit')]
#              listOfOptions.append(option)
# logger.debug(option.tag + ": "+ option.text +"["+  option.get('unit')+"]");
#            return self.success({"data": listOfOptions})
#
#
    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def loadModuleOptions(self, unique_id, pathToBootstrap):

        r = self._augeroffline(unique_id)
        r.loadModuleOptions(pathToBootstrap)
